package src;

/** Programa Convertidor
 * @Charles Stna
 * @Version 0.1
 */


public class Main {
    public static void main(String[] args) {
        System.out.println("Convertidor del Temperatura \n en Números enteros \n");
        int valor = 80;
        int resultado = (int) ConvertidorCF(valor);
        System.out.println("O Valor de "+valor+"ºF é : "+resultado+"ºC\n ");
    }
    public static double ConvertidorCF ( double fahrenheit) {
        return ((fahrenheit - 32.0) * 5) / 9;
    }
}